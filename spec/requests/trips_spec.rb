require 'rails_helper'

RSpec.describe 'Trips API', type: :request do
  before { host! "api.dev.localhost" }
  # initialize test data
  let!(:trips) { create_list(:trip, 10) }
  let(:trip_id) { trips.first.id }

  # test suite for GET /trips/:id
  describe 'GET /trips/:id' do
    before { get "/trips/#{trip_id}" }

    context 'when the record exists' do
      it 'returns the trip' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(trip_id)
      end
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:trip_id) { 100 }
      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Trip/)
      end
    end
  end


  # test suite for POST /trips
  describe 'POST /trips' do
    let(:valid_attributes) { { trip: { status: 'ongoing' } } }

    context 'when the request is valid' do
      before { post '/trips', params: valid_attributes }

      it 'creates a trip' do
        expect(json['status']).to eq('ongoing')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is not valid' do
      before { post "/trips", params: {} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
      it 'returns a validation failure message' do
        expect(response.body).to match("{\"message\":\"Validation failed: Status can't be blank\"}")
      end
    end
  end

  # Test suite for PUT /trips/:id
  describe 'PUT /trips/:id' do
    let(:valid_attributes) { { trip: { status: 'completed' } } }
    context 'when the record exists' do
      before { put "/trips/#{trip_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # test suite for DELETE /todos/:id
  describe 'DELETE /trips/:id' do
    before { delete "/trips/#{trip_id}" }
    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
