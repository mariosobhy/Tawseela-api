module RequestSpecHelper
  # Parse json responses to ruby hash
  def json
    JSON.parse(response.body)
  end
end
