# Tawseela API
In our platforms, we often need to track ongoing bus trips. We use this data to
provide real time information to our users. We also analyze the historical trips
location data to come up with conclusions about the routes efficiency and
recommendations for new routes.	


## Installation

- Install `ruby 2.3.1`
- Run `bundle install` to get the required dependencies
- Run `bundle exec rake db:create`, `bundle exec rake db:migrate`, and `bundle exec rake db:seed` to set up the database
- Run the server `rails server`
- Run the test suite using `bundle exec rspec`
- Run rubocop using `bundle exec rubocop` to check code style

## Features 

#### Rspec testing 
  I used rspec testing framework to perform integration tests, and are designed
  to drive behavior through the full stack, including routing(provided by Rails)
#### Serializing 
  I used active model serializers to make it easier to format JSON responses
  when using Rails as API server 
#### Caching
  caching can really provide great benfits by speeding up your APIs so I used it
  to improve request/response time consuming from 0m0.127s approximately to
  0m0.017s approximately 
#### Background jobs
  I used active job integrated with sidekiq to solve refresh cache after get
  updated , so I automated warm up the cache before the user hits the controller
  action (index) 
#### Postman
	I used postman to test API request and here's a shared collection
	[Tawseela API With Postman](https://www.getpostman.com/collections/61947a35959e3c1f65e4)
	
## Branches 	

#### Master
		In the main branch I implemented 
		- CRUD for the trip and location.
		- Update trip status: the status can only changed in one direction. 
		- Submit location to ongoing trip and keep the full location history of
		  the trip, and not only the last location
		- Active job with sidkiq to wrap cache on every post or update(trip) before the user hits the controller
			action (index)  

#### feature/add_caching_t	o_api  
		In this branch I implemented 
		- Active job with sidkiq to wrap cache before the user hits the controller
			action (index)
