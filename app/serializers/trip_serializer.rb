class TripSerializer < ActiveModel::Serializer
  # attributes to be serialized
  attributes :id, :status, :created_at, :updated_at

  has_many :locations
end
