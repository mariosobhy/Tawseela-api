module API 
  class ApplicationController < ActionController::API
    include Response
    include ExceptionHandler
  end
end 
