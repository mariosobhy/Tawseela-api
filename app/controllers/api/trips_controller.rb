module API 
  class TripsController < ApplicationController
    before_action :set_trip,only: [:show,:update,:destroy]

    # GET /trips
    def index
      trips = Trip.includes(:locations)
      json = Rails.cache.fetch(Trip.cache_key(trips)) do
        trips.to_json(include: :locations)
      end

      render json: json
    end

    # POST /trips
    def create
      @trip = Trip.create!(trip_params)
      render json: @trip.as_json(:include => :locations),status: :created
    end

    # GET /trips/:id
    def show
      render json: @trip.as_json(:include => :locations), status: 200
    end

    # PUT /trips/:id
    def update
      @trip.update!(trip_params)
      head :no_content
    end

    # DELETE /trips/:id
    def destroy
      @trip.destroy
      head :no_content
    end

    private
    def trip_params
      params.require(:trip).permit(:status,:locations_attributes => [:latitude, :longitude])
    end
    def set_trip
      @trip = Trip.find(params[:id])
    end
  end
end 
