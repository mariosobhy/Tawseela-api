class Trip < ApplicationRecord
  #-------------------------------- Associations -----------------------------------
  has_many :locations, inverse_of: :trip, dependent: :destroy
  accepts_nested_attributes_for :locations, reject_if: :all_blank

  #-------------------------------- Enums -----------------------------------
  # define enum for trip status as ongoing or completed
  enum status: {ongoing: 0,completed: 1}

  #-------------------------------- Validations -----------------------------------
  validates_presence_of :status
  #validate :location_exist
  # status will not changed after completed
  validate :status_can_not_changed_after_completed, on: :update
	
	#-------------------------------- Callbacks ---------------------------------
	after_save :create_json_cache

	def status_can_not_changed_after_completed
		if status == "ongoing" && status_was == "completed"
			errors.add(:status, "trip status cannot changed after completed")
		end
	end
=begin
	def location_exist 
		# test existing location for each update
		errors.add(:locations, "location must exist") if locations.blank? && status == "ongoing"
	end 
=end   
	def self.cache_key(trips) 
		{
			serializer: 'trips',
			stat_record: trips.maximum(:updated_at)
		}
	end 

	def create_json_cache 
		CreateTripsJsonCacheJob.perform_later
	end 
end
